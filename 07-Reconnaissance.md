# Section-7 Passive and active reconnaissance

To collect as much information as possible. Collecting information and knowing deeply about the target system is known as **“Reconnaissance”**. This data is the main street for the hackers to hack the target system. It involves Footprinting, Enumeration, and Scanning.
<br><br>

> Seven steps of reconnaissance
1. Gather initial information
2. Determine the network range
3. Identify active machines
4. Discover open ports and access points
5. Fingerprint the operating system
6. Uncover services on ports
7. Map the network
<br><br>

> Passive vs. Active reconnaissance

|                      Passive reconnaissance                      |                      Active reconnaissance                      |
|:----------------------------------------------------------------:|:---------------------------------------------------------------:|
|1. Gathering information without actually connecting to the target.|1. Gathering information about the target while initiating connections.|
|2. Passive reconnaissance generally involves Open Source Intelligence (OSINT) investigation.|2. Active reconnaissance typically involves using of varity of tools like port and vulnerability scanners.|
|3. This allow not being detected while gathering information.|3. This allow you to gather more information but allow you to be detected.|
<br>

## Passive Reconnaissance

### Recon-ng

Recon-ng is one of the most powerful tool for passive reconnaissance, it is generally the first tool on your list if you want to gather information about your target using OSINT.

**You can know about:**
- IP addersses
- Account names conventions
- Loactions
- Users
- Email addresses
- Passwords
- And more...!

**Usage of Recon-ng**
```sh
# Installing Recon-ng

sudo git clone https://github.com/LaNMaSteR53/recon-ng.git /opt/recon-ng

-----------------------------------------------------------
# Run Recon-ng

cd /opt/recon-ng
./recon-ng

# Create a workspace
> workspace create "[name of workspace]"

# Search Modules
> marketplace search

# Install Module
> marketplace install hackertarget

# Load Module
> modules load hackertarget

# Get module information
> info

# See and change options
> options list
> options set SOURCE google.com

# Run
> run

# Exit from module
> back

-----------------------------------------------------------
# Important Modules
1. hackertarget
2. google_site_web
3. netcraft
4. wohis_pocs
5. pgp_search
6. hibp_paste
7. html
```
<br>

### Whois Enumeration

Whois is a tool comes bulit-in in kali linux that can help you finding information about a particular domain.
- Its registrar
- Their name servers
- Info. of DNS security
- Owner's info.
- Registration info.

**Usage**
```sh
whois "[domain]"
```
<br>

### DNS Enumeration

The tool used for DNS enumeration is dnsrecon and their is a website as well for this (searchdns.netcraft.com)...

**Usage**
```sh
dnsrecon -d "[domain]" -g (google search)
```

### Search DNS

url : https://searchdns.netcraft.com/

Netcraft SearchDNS is a useful tool that can give you an insight on DNS information for given domain.
- Names of all subdomains that exist for the domain.
- Who owns that domain.
- IP addresses of the server.
- Nameserver addresses.
- DNS admin info.
- Where it is hosted.
<br><br>

### Exploit_db

url : https://www.exploit-db.com/

- Google hacking database aka. exploit_db is a collection of various google dorking commands and combinations that updates by different users around the globe on a daily bases.
- You can use google dork commands to search for a specific thing on the internet.
- Google dorking increases the possibilities of finding the right information about a particular target by just using google search.
<br><br>

### Shodan

url : https://www.shodan.io/

**Shodan** is also an OSINT tool. It is also known as *Hacker's Google* beacuse it can get information about every singke device that is connected to the internet. You can find vulnerable devices in shodan.
<br><br>

### Securityheaders

url : https://securityheaders.com/

**Securityheaders** is a tool that allows you to check if a site has its security headers setuo correctly to prevant attacks.
<br><br>

### SSLlabs

url : https://www.ssllabs.com/ssltest/

**SSLlabs** tests the server to see if its SSL settings are correctly set and will tell you if it accepts a vulnerable version of TLS.
<br><br>

### Pastebin

url : https://pastebin.com/

**Pastebin** is a website where people around the globe dumps information and anyone can view them.
<br>

-------------------------------------------------------------------------------------------------------------------------------
<br>

## Active Reconnaissance

### NMAP

- Nmap is probably one of the most important tools in the pentesting tool kit.
- It is a versatile port scanner that can be used to scan entire networks to discover what ports are open.
- Information that nmap can gather includes:
    - Operating system
    - Services running on device
    - Open/closed ports
    - Vulnerabilities on that device

**Usage Nmap**
```md
# TCP syn-awk scan

> nmap -sS [Network]

----------------------------------------------------
# All port scan

> nmap -p- [IP_Adderss]

----------------------------------------------------
# Enumerate versions

> nmap -sV [IP_Adderss]

----------------------------------------------------
# Aggressive OS enumeration

> nmap -A -O [IP_Adderss]

----------------------------------------------------
# Recommended 

> nmap -sC -sV [IP_Adderss]

----------------------------------------------------
# Nmap Scripting Engine(NSE)

> nmap --script=vuln [IP_Adderss]
```
<br>

### Netcat

- Netcat is known as the swiss army khife for hackers.
- It is essential for remote connections to exploited devices.
- It can also br used as a port scanner in some scenarios.

**Usage**

*Netcat as a port scanner*
```sh
nc -nvv -w 1 -z [IP_Adderss] 1-100 | grep "open"
```

*Scanning UDP ports*
```sh
nc -nvv -w 1 -z -u [IP_Adderss] 1-100 | grep "open"
```

*Netcat listener*
```sh
nc -nlvp [open port]
```

### SMB Enumeration

- Server Message Block(SMB) enumeration is key to information gathering in corporate environments.
- SMB is used typically for file transfer over ports 139/445 and is often easy to exploit.
- On some devices, if you can exploit SMB, you can own the entire device.
- SMB enumeration can be done with vairety of tools including nmap.

*Nmap Syntax*
```md
# nmap -v -p 139,445 -oN smb.txt [IP_address]
```

*Eternalblue Scan*
```md
# nmap -v --script=vuln-ms17-010.nse [IP_address]
```
<br>

### NFS Enumeration

- Network File Share(NFS) enumeration is another key thing to lookout for when gathering information on a network.
- NFS runs over port 111 is typically used to mount network share and can potentially be exploited if setup incorrectly.
- We can enumerate NFS information with namp.

*Nmap Syntax*
```md
# nmap -sV -p 111 -vv [IP adress] --script=rpcinfo
```
<br>

### Nikto

- Nikto is a useful web application scanner that can find vulnerablities and weeknesses that exist on web servers.
<br><br>

### Sparta

- Sparta is a useful tool that combiles multiple tools togeather into one interface for faster, more automated information gathering.
- It includes 
    - nmap
    - nikto
    - hydra
    - a screenshot tool
- You can conduct port scans, web application scans, and bruteforce all just by giving the software an IP address.

*Spatra is not present in latest version of kali linux, so we are using legion(Best alternative for sparta)*

**Usage**
```sh
# Install Legion
sudo apt install legion

# Run legion
legion

"""Legion will pop up a GUI to use it..."""

# You just have to enter the IP address into host(s) to scope panel
```
<br>

### SMTP Enumeration

- Simple Mail Transfer Protocol(SMTP) runs over port 25 and can be used to gather information if that port is open.
- We can connect to that port with netcat and send commands to verify if specific users exist or not.

*Netcat Syntax*
```sh
nc -nv [IP address] 25
```

*After connecting SMTP Syntax*
```sh
# VRFY to verify if the user exist
VRFY root
```

*Python Script for SMTP Enumeration*
```python
#!/usr/bin/python

import socket
import sys

if len(sys.argv) != 2:
    print("Usage: smtp.py <username>")
    sys.exit(0)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
connect = s.connect(('192.168.1.13',25))

banner = s.recv(1024)
print(banner)

s.send('VRFY '+sys.argv[1] +'\r\n')

result = s.recv(1024)
print(result)

s.close
```
<br>

### Nessus Vulnerability Scanner

- Nessus is a vulnerability scanner that allows you to scan an entire network to discover vulnerabilities that exist on devices.
- This is a paid software but they have a free version that limits the number of IPs that you can scan.
<br><br>

### OpenVAS Vulnerability Scanner

- OpenVAS is an open source vulnerability scanner that is free to the public.
- While it is not robust or built out as Nessus, it can be a solid vulnerability scanner if you set it up right and use it correctly.