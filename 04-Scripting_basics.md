# Section-4 Scripting basics
Bash/Python/Powershell
--------------------------------------------------------

> What is an operating system?

- A software that directly manages system's hardware
- It sits between application and hardware
- It is like an interpreter.

```
                [USER] ---> [APPLICATION] ---> [OPERATING SYSTEM] ---> [HARDWARE] 

                                      --->||DATA FLOW||<---

                [USER] <--- [APPLICATION] <--- [OPERATING SYSTEM] <--- [HARDWARE] 
```

> What is scripting language?

A programming language designed for integration and communicating with other programming languages.
- Bash
- Batch
- Python
- Powershell
<br><br>

> What is linux?

An open source, free operating system that can run on pretty much any hardware. It is very lightweight and very secure OS. It tipically focused on CLI.
<br>

**Basic linux command:**
```
whoami   (username of current user)
hostname (username of current user)
pwd      (print working directory)
ls       (list files and directory)
cd       (change directory)
cd ..    (change to parent directory)
cd ../.. (change to upto 2 parent directory)
mkdir    (make directory)
rm       (remove file and directory)               
rmdir    (remove directory)
su       (switch user | super user)   
sudo     (super user do)
sudo -i  (super user users) 
history  (see all previous commands)
ifconfig (see info about NIC)
ip addr  (see info about NIC)
chmod    (change permissions)
touch    (create a file)
```

> Important directories:
```
/          ----->   root directory
/etc       ----->   system configuration files
/bin       ----->   binary files, application
/sbin      ----->   binary files, application (super user)
/var/logs  ----->   logs stored
/home      ----->   home directory
```

> Install & Update:
```
apt-get install
apt-get update
apt-get upgrade
```

> Linux text editors:

- vi
- vim
- nano
- leafpad
<br>

> Bash scripting:
```sh
#!/bin/bash ---> (shebang)

VariableName = Value
(To access it) ---> $VariableName

echo ---> (print)

Create a function:
print_me() {
    echo "Print this"
}

User input (capture)
-> read -p
-> read -sp (silently)
```

> Bash (port scanner)
```sh
#!/bin/bash
if ["$1" == "" ]
then
  echo "Usage: ./port.sh [IP]"
  echo "Example ./port.sh 192.168.1.10"
else
  echo "Please wait while it is scanning all the open ports..."
  nc -nvz $1 1-65535 > $1.txt 2>&1
fi
tac $1.txt
rm -rf $1.txt
```

> Python scripting: (Net Tester):
```python
import socket
import os

print("[A] Network Scanner\n[B] Port Scanner\n[C] Both\n[D] Exit")
option = input('Enter your option: ').upper()

if option == 'A':

    print('\nOpening NetScanner...!')
    print('Input example: 10.1.1\n')
    def commands(a):
        r = int(input("Enter the scope of scan: "))
        for i in range(1,r+1):
            c = f'ping -n 1 {a}.{i} | grep "TTL" | cut -d " " -f 3 | sed \'s/.$//\''
            os.system(c) 
    commands(input('Enter network: '))


elif option == 'B':
    
    print('Opening PortScanner...!\n')
    print('Input example: 192.168.1.10\n')
    def port_sacnner(ip):
        c = input("Recommended || Do you want to use defult ports scan[Y/N]: ").upper()

        if c == 'Y':
            l =[20,21,22,23,25,53,67,68,69,70,79,80,88,110,123,143,144,443,465,587,520,666,993,995,3389,139,445,161,389,105,106,2224,3360]
            for i in l:
                port = i
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                if s.connect_ex((ip,port)):
                    pass

                else:
                    print(f"{port} is open")
        
        elif c == 'N':
            start= int(input("Enter Starting Point: "))
            end= int(input("Enter Ending Point: "))
            l = []
            for num in range(start,end+1):
                l.append(num)
            for i in l:
                port = i
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                if s.connect_ex((ip,port)):
                    pass

                else:
                    print(f"{port} is open")
        
        else:
            print('Invalid Option')
    port_sacnner(input('Enter your IP: '))

elif option == 'C':

    print('\nOpening NetScanner...!')
    print('Input example: 10.1.1\n')
    def commands(a):
        r = int(input("Enter the scope of scan: "))
        for i in range(1,r+1):
            c = f'ping -n 1 {a}.{i} | grep "TTL" | cut -d " " -f 3 | sed \'s/.$//\''
            os.system(c)
    commands(input('Enter network: '))

    print('Opening PortScanner...!\n')
    print('Input example: 192.168.1.10\n')
    def port_sacnner(ip):
        c = input("Recommended || Do you want to use defult ports scan[Y/N]: ").upper()

        if c == 'Y':
            l =[20,21,22,23,25,53,67,68,69,70,79,80,88,110,123,143,144,443,465,587,520,666,993,995,3389,139,445,161,389,105,106,2224,3360]
            for i in l:
                port = i
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                if s.connect_ex((ip,port)):
                    pass

                else:
                    print(f"{port} is open")
        
        elif c == 'N':
            start= int(input("Enter Starting Point: "))
            end= int(input("Enter Ending Point: "))
            l = []
            for num in range(start,end+1):
                l.append(num)
            for i in l:
                port = i
                s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

                if s.connect_ex((ip,port)):
                    pass

                else:
                    print(f"{port} is open")
        
        else:
            print('Invalid Option')
    port_sacnner(input('Enter your IP: '))

else:
    exit()
```