# Section-1 Introduction

> Cybersecurity Market:

- Hacking attacks in every 39 seconds.
- After covid-19 FBI reported 300% increase in cyber crimes.
- 43% of cyber attacks target small business.
- 95% of breache occurs in :
    - Government sector
    - Retail business
    - Tech industry
    - Financial sector
- Approx. 6 trillion $ expected to be spent globally on cybersecurity by 2021.
<br><br>

> 3 types of hackers:

* **White Hat** : Good guys/ethical hackers.
* **Black Hat** : Bad guys/Threat actors.
* **Gray Hat** : Somewhere in between white and black hat.
<br><br>

> 4 elements of security:

*All attacks are an attempt to breach computer system security.*
<br>

1. Confidentiality
2. Authenticity
3. Integrity
4. Availability
<br><br>

> Ethical hacking terminology:

* **Threat** : A situation that could lead to a potential breach of security.
* **Exploit** : A piece of software that takes the asvantages of any system weakness.
* **Vulnerability** : System weakness/flaw.
* **Target of Evalaution (TOE)** : System/network subject to security analysis.
* **Remote** : Exploit sent over a network.
* **Local** : Exploit delivered directly to computer system.
* **Attack** : System compromised baces on a vulnerability.
<br><br>

> Common methods:

01. Virus/Trojan(*Malware*)
02. Phishing(*Replicate website*)
03. Eaves dropping(*Monitoring without you knowing*)
04. Fake AP(*Rogue Access Point*)
05. Waterhole(*Location specific attack*)
06. DDOS(*Traffic flooding*)
07. Keylogger(*Track key_strocks*)
08. Social Engineering(*Hacking psychologically*)
09. Bait & Switch(*fake ads*)
10. Cookie theft(*Take cookies off your computer*)
<br><br>

> Cybersecurity vs. Ethical Hacking:
<br>

|**Cybersecurity**|**Ethical Hacking**|
|:---------------:|:-----------------:|
|Cybersecurity put simply the act of taking certain mesures to ensure that a netwirk and its information is safe.|Ethical hacking is the act of attempting to breach into a network in order to uncover vulnerabilities that may be present.|
|Cybersecurity focuses on protection and defensive mesures.|Ethical hacking focuses on offensive actions.|
|Cybersecurity is a broader term.|Ethical hacking falls under cybersecurity.|
<br><br>

**Ethical hacking and Penetration testing are actully the same**
<br><br>

> Types of ethical hacking:

1. **White Box -** *Having complete knowledge about the target.* 
2. **Gray Box -** *Having some knowledge about the target.*
3. **Black Box -** *Having no knowledge about the target.*
<br><br>

> Types of pentesting:

1. Internal Pentesting
2. External Pentesting
3. Web Application Pentesting