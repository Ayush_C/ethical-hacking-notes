# Section-2 Networking Basics

A network occurs when two or more computers are linked together and can share resourses.
<br><br>

> Types of networks:
1. **LAN** (*Local Area Network*)
2. **MAN** (*Metropolitan Area Network*)
3. **WAN** (*Wide Area Network*)
4. **PAN** (*Personal Area Network*)
5. **CAN** (*Campus Area Network*)
6. **SAN** (*Storage Area Network*)
7. **VPN** (*Virtual Private Network*)
<br><br>

> Permissions and Network Security:

- If a machine is on a network, it doesn't automatically means that every other machine and device has access to it.
- Permissions and security are central to the idea of networking.
- With hacking, you are able to gain inauthorized access to a computer.
<br><br>

> What makes a network:

To make a network you need nodes and connections. In offices, wired connections are still more common beacuse they are generally fast. To establish a connection we need a NIC (*Network Interface Card*), and each NIC have its own seperate numaric identifier called MAC (*Media Access Conrtol*).
<br><br>

> Internet as a network:

Internet is a best example of WAN (Public WAN) i.e. internet is publically accessable.
<br><br>

> IP Address:

IP address is a unique identification number of a particular device in a newtork, no two computers can have a same IP address in a network.
<br><br>

> How data travels accross newtorks:

- In form of packets
- Each packet have two address [ Source Address | Destination Address ]
- Router reads those addresses and send the packets to the correct address.
<br><br>

> Ports & Protocols:

*Ports are special identification numbers of services.*
<br>

**Ports you need to know:**
01. FTP - 20(data), 21(control)
02. SSH - 22
03. Telnet - 23
04. SMTP - 25
05. DNS - 53
06. DHCP - 67(server), 68(client) 
07. TFTP - 69
08. Gopher - 70
09. HTTP - 80
10. Kerberos - 88
11. POP3 - 110
12. NTP - 123
13. IMAP - 143
14. NEWS - 114
15. HTTPS - 443
16. SMTPS - 465(SSL), 587(TLS)
17. RIP - 520
18. DOOM - 666
19. IMAPS - 993
20. POP3S - 995
21. SMB - 139(windows), 445(linux)
22. SNMP - 161
23. LDAP - 389 (active directory)
<br><br>

> TCP & UDP:

* Transmission Control Protocol | Connection Oriented | more secure...
* User Datagram Protocol | Connection Less | fatser delivery...
<br><br>

> Private IP address:

* **A** - 10.0.0.1 --> 10.255.255.254
* **B** - 172.16.0.1 --> 172.31.255.254
* **C** - 192.168.0.1 --> 192.168.255.254
