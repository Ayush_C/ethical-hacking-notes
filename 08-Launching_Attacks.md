# Section-8 Launching Attacks

> Phases of an Attack

1. Reconnaissance
2. Scanning
3. Gaining access
4. Maintaining accsess
5. Covering tracks

**Phase-1 Reconnaissance**

*Perp phase where an attacker gathers info about the target prior to launching the attack.*

*Reconnaissance types:*

* **Passive** : Attacker does not interact with the system directly.
* **Active** : Attacker interacts with system by using tools to gain important info about the system.
<br>

**Phase-2 Scanning**

- Attacker uses the details gathered during reconnaissance to identify specific vulnerabilities.
- An attacker can gather critical information, such as the mapping of system, routers, and firewalls.
- **Port scanners** can be used to detect listening ports to find information about the nature of services running on the target machine.
- **Vulnerability scanners** (most common tools)
<br>

**Phase-3 gaining Access**

- Where most of the damage is usually done.
- Access can be gained locally, offline, over a LAN, or over the internet.
- A hacker's chances of gaining access into a target system are influenced by factors such as:
    - Architecture and configuration of the taget system.
    - Skill level of the hacker.
    - Initial level of access obtained.
<br>

**Phase-4 Maintaining Access**

- Attacker, who choose to remain undetected.
    - Remove any evidence of their intrusion.
    - Install a backdoor or a torjan to gain repeat access.
    - Install **rootkits** to gain full administrator access to the target computer.
- Hacker can use **Trojan horses** to transfer username, password, and any other information stored on the system.
- Organizations can use intrusion detection systems or deploy traps knowns as **honeypots** and honeynets to detect intruders.
<br>

**Phase-5 Covering Tracks**

- A good hacker will usually erase all evidence of their actions.
- Trojans such as ps or netcat are often used to erase the attacker's activities from thet system's log files.
* **Steganography**
    - Process of hiding data in other data, for instance image and sound files.
* **Tunneling**
    - Takes advantages of the transmission protocol by carrying one protocol over another.
<br>

## Social Engineering

Social engineering is the art of exploiting human psychology, rather than technical hacking techniques, to gain access to buildings, systems or data.

**Common social engineering attacks:**
- **Phishing** - Attackers are using emails, social media, etc., to trick victims to provide sensitive information or visiting malicious URLs.

- **Watering hole** - Injecting malicious code into public web pages of a site that target used to visit.

- **Pretexting** - Hacker presenting themselves as someone else in order to obtain private information.
<br><br>

## Analyzing gathered information

The first step of launching attack is to analyze all the information that was gathered during reconnaissance. This includes looking at the results of various scans to complie lists of:

* IP Addresses
* OPerating System and Hostnames
* Open ports
* Discovered network shares
* Discovered user accounts
* Potential Vulnerabilities that were discovered
<br>

```md
# Host discovery

C:\Users\ASUS>nmap -sn 192.168.1.0/24
Starting Nmap 7.91 ( https://nmap.org ) at 2021-04-07 23:59 India Standard Time
Nmap scan report for 192.168.1.1
Host is up (0.00s latency).
MAC Address: F8:C4:F3:39:3C:50 (Shanghai Infinity Wireless Technologies)
Nmap scan report for 192.168.1.7
Host is up (0.078s latency).
MAC Address: 2E:A0:5F:B1:C0:00 (Unknown)
Nmap scan report for 192.168.1.17
Host is up (0.093s latency).
MAC Address: 5E:67:0A:F3:D4:63 (Unknown)
Nmap scan report for 192.168.1.13
Host is up.
Nmap done: 256 IP addresses (4 hosts up) scanned in 10.29 seconds

-----------------------------------------------------------
# TCP-SYN

C:\Users\ASUS>nmap -sS 192.168.1.13
Starting Nmap 7.91 ( https://nmap.org ) at 2021-04-08 00:01 India Standard Time
Nmap scan report for 192.168.1.13
Host is up (0.00013s latency).
Not shown: 986 closed ports
PORT     STATE SERVICE
21/tcp   open  ftp
25/tcp   open  smtp
79/tcp   open  finger
80/tcp   open  http
106/tcp  open  pop3pw
110/tcp  open  pop3
135/tcp  open  msrpc
139/tcp  open  netbios-ssn
143/tcp  open  imap
443/tcp  open  https
445/tcp  open  microsoft-ds
902/tcp  open  iss-realsecure
912/tcp  open  apex-mesh
3306/tcp open  mysql

Nmap done: 1 IP address (1 host up) scanned in 1.28 seconds

-----------------------------------------------------------
# Agressive scan || OS and version enumeration || All port scan

PS C:\Users\ASUS> nmap -A -O -p- 192.168.1.13
Starting Nmap 7.91 ( https://nmap.org ) at 2021-04-08 15:44 India Standard Time
Nmap scan report for 192.168.1.13
Host is up (0.00s latency).
Not shown: 65511 closed ports
PORT      STATE    SERVICE         VERSION
21/tcp    open     ftp             FileZilla ftpd 0.9.41 beta
| ftp-syst:
|_  SYST: UNIX emulated by FileZilla
25/tcp    open     smtp            Mercury/32 smtpd (Mail server account Maiser)
| smtp-commands: localhost Hello nmap.scanme.org; ESMTPs are:, TIME, SIZE 0, HELP,
|_ Recognized SMTP commands are: HELO EHLO MAIL RCPT DATA RSET AUTH NOOP QUIT HELP VRFY SOML Mail server account is 'Maiser'.
79/tcp    open     finger          Mercury/32 fingerd
| finger: Login: Admin         Name: Mail System Administrator\x0D
| \x0D
|_[No profile information]\x0D
80/tcp    open     http            Apache httpd 2.4.41 (OpenSSL/1.1.1c PHP/7.3.9)
| http-ls: Volume /
| SIZE  TIME              FILENAME
| 1.0G  2021-02-23 00:14  Pokemon.Detective.Pikachu.2019.720p.Hindi-English.MoviesFlixPro.in.mkv
|_
| http-methods:
|_  Potentially risky methods: TRACE
|_http-server-header: Apache/2.4.41 (Win64) OpenSSL/1.1.1c PHP/7.3.9
|_http-title: Index of /
105/tcp   open     ph-addressbook  Mercury/32 PH addressbook server
106/tcp   open     pop3pw          Mercury/32 poppass service
110/tcp   open     pop3            Mercury/32 pop3d
|_pop3-capabilities: EXPIRE(NEVER) TOP USER UIDL APOP
135/tcp   open     msrpc           Microsoft Windows RPC
137/tcp   filtered netbios-ns
139/tcp   open     netbios-ssn     Microsoft Windows netbios-ssn
143/tcp   open     imap            Mercury/32 imapd 4.62
|_imap-capabilities: OK IMAP4rev1 complete X-MERCURY-1A0001 CAPABILITY AUTH=PLAIN
443/tcp   open     ssl/http        Apache httpd 2.4.41 (OpenSSL/1.1.1c PHP/7.3.9)
| http-ls: Volume /
| SIZE  TIME              FILENAME
| 1.0G  2021-02-23 00:14  Pokemon.Detective.Pikachu.2019.720p.Hindi-English.MoviesFlixPro.in.mkv
|_
| http-methods:
|_  Potentially risky methods: TRACE
|_http-server-header: Apache/2.4.41 (Win64) OpenSSL/1.1.1c PHP/7.3.9
|_http-title: Index of /
| ssl-cert: Subject: commonName=localhost
| Not valid before: 2009-11-10T23:48:47
|_Not valid after:  2019-11-08T23:48:47
|_ssl-date: TLS randomness does not represent time
| tls-alpn:
|_  http/1.1
445/tcp   open     microsoft-ds?
902/tcp   open     ssl/vmware-auth VMware Authentication Daemon 1.10 (Uses VNC, SOAP)
912/tcp   open     vmware-auth     VMware Authentication Daemon 1.0 (Uses VNC, SOAP)
2224/tcp  open     http            Mercury/32 httpd
|_http-title: Mercury HTTP Services
3306/tcp  open     mysql           MariaDB (unauthorized)
5040/tcp  open     unknown
49664/tcp open     msrpc           Microsoft Windows RPC
49665/tcp open     msrpc           Microsoft Windows RPC
49666/tcp open     msrpc           Microsoft Windows RPC
49667/tcp open     msrpc           Microsoft Windows RPC
49668/tcp open     msrpc           Microsoft Windows RPC
49677/tcp open     msrpc           Microsoft Windows RPC
Device type: general purpose
Running: Microsoft Windows 10
OS CPE: cpe:/o:microsoft:windows_10
OS details: Microsoft Windows 10 1809 - 1909
Network Distance: 0 hops
Service Info: Hosts: localhost, www.example.com; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-security-mode:
|   2.02:
|_    Message signing enabled but not required
| smb2-time:
|   date: 2021-04-08T10:17:37
|_  start_date: N/A

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 192.98 seconds
```

```md
# Recommend 

PS C:\Users\ASUS\OneDrive\Desktop> nmap -sC -sV 192.168.1.13
Starting Nmap 7.91 ( https://nmap.org ) at 2021-04-08 15:58 India Standard Time
Nmap scan report for 192.168.1.13
Host is up (0.000012s latency).
Not shown: 986 closed ports
PORT     STATE SERVICE         VERSION
21/tcp   open  ftp             FileZilla ftpd 0.9.41 beta
| ftp-syst:
|_  SYST: UNIX emulated by FileZilla
25/tcp   open  smtp            Mercury/32 smtpd (Mail server account Maiser)
| smtp-commands: localhost Hello nmap.scanme.org; ESMTPs are:, TIME, SIZE 0, HELP,
|_ Recognized SMTP commands are: HELO EHLO MAIL RCPT DATA RSET AUTH NOOP QUIT HELP VRFY SOML Mail server account is 'Maiser'.
79/tcp   open  finger          Mercury/32 fingerd
| finger: Login: Admin         Name: Mail System Administrator\x0D
| \x0D
|_[No profile information]\x0D
80/tcp   open  http            Apache httpd 2.4.41 (OpenSSL/1.1.1c PHP/7.3.9)
| http-ls: Volume /
| SIZE  TIME              FILENAME
| 1.0G  2021-02-23 00:14  Pokemon.Detective.Pikachu.2019.720p.Hindi-English.MoviesFlixPro.in.mkv
|_
| http-methods:
|_  Potentially risky methods: TRACE
|_http-server-header: Apache/2.4.41 (Win64) OpenSSL/1.1.1c PHP/7.3.9
|_http-title: Index of /
106/tcp  open  pop3pw          Mercury/32 poppass service
110/tcp  open  pop3            Mercury/32 pop3d
|_pop3-capabilities: TOP APOP EXPIRE(NEVER) UIDL USER
135/tcp  open  msrpc           Microsoft Windows RPC
139/tcp  open  netbios-ssn     Microsoft Windows netbios-ssn
143/tcp  open  imap            Mercury/32 imapd 4.62
|_imap-capabilities: CAPABILITY OK X-MERCURY-1A0001 AUTH=PLAIN complete IMAP4rev1
443/tcp  open  ssl/http        Apache httpd 2.4.41 (OpenSSL/1.1.1c PHP/7.3.9)
| http-ls: Volume /
| SIZE  TIME              FILENAME
| 1.0G  2021-02-23 00:14  Pokemon.Detective.Pikachu.2019.720p.Hindi-English.MoviesFlixPro.in.mkv
|_
| http-methods:
|_  Potentially risky methods: TRACE
|_http-server-header: Apache/2.4.41 (Win64) OpenSSL/1.1.1c PHP/7.3.9
|_http-title: Index of /
| ssl-cert: Subject: commonName=localhost
| Not valid before: 2009-11-10T23:48:47
|_Not valid after:  2019-11-08T23:48:47
|_ssl-date: TLS randomness does not represent time
| tls-alpn:
|_  http/1.1
445/tcp  open  microsoft-ds?
902/tcp  open  ssl/vmware-auth VMware Authentication Daemon 1.10 (Uses VNC, SOAP)
912/tcp  open  vmware-auth     VMware Authentication Daemon 1.0 (Uses VNC, SOAP)
3306/tcp open  mysql           MariaDB (unauthorized)
Service Info: Hosts: localhost, www.example.com; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
| smb2-security-mode:
|   2.02:
|_    Message signing enabled but not required
| smb2-time:
|   date: 2021-04-08T10:29:06
|_  start_date: N/A

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 29.92 seconds
```

```md
# Nmap Scripting Engine(NSE)

PS C:\Users\ASUS\OneDrive\Desktop> nmap --script=vuln 192.168.1.13
Starting Nmap 7.91 ( https://nmap.org ) at 2021-04-08 16:02 India Standard Time
Pre-scan script results:
| broadcast-avahi-dos:
|   Discovered hosts:
|     224.0.0.251
|   After NULL UDP avahi packet DoS (CVE-2011-1002).
|_  Hosts are all up (not vulnerable).
Nmap scan report for 192.168.1.13
Host is up (0.0011s latency).
Not shown: 986 closed ports
PORT     STATE SERVICE
21/tcp   open  ftp
|_sslv2-drown:
25/tcp   open  smtp
| smtp-vuln-cve2010-4344:
|_  The SMTP server is not Exim: NOT VULNERABLE
|_sslv2-drown:
79/tcp   open  finger
80/tcp   open  http
| http-csrf:
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.1.13
|   Found the following possible CSRF vulnerabilities:
|
|     Path: http://192.168.1.13:80/project/Gaming/index.html
|     Form id: idformregister
|     Form action: https://www.themvp.in/index.php?route=account/register/ajax
|
|     Path: http://192.168.1.13:80/project/Login_Form/index.html
|     Form id:
|     Form action: ../index.html
|
|     Path: http://192.168.1.13:80/project/Registration/index.html
|     Form id:
|_    Form action: ../Login_form/index.html
|_http-dombased-xss: Couldn't find any DOM based XSS.
| http-enum:
|   /: Root directory w/ listing on 'apache/2.4.41 (win64) openssl/1.1.1c php/7.3.9'
|   /phpmyadmin/: phpMyAdmin
|   /icons/: Potentially interesting folder w/ directory listing
|   /licenses/: Potentially interesting directory w/ listing on 'apache/2.4.41 (win64) openssl/1.1.1c php/7.3.9'
|   /server-info/: Potentially interesting folder
|_  /server-status/: Potentially interesting folder
| http-fileupload-exploiter:
|
|     Couldn't find a file-type field.
|
|_    Couldn't find a file-type field.
| http-slowloris-check:
|   VULNERABLE:
|   Slowloris DOS attack
|     State: LIKELY VULNERABLE
|     IDs:  CVE:CVE-2007-6750
|       Slowloris tries to keep many connections to the target web server open and hold
|       them open as long as possible.  It accomplishes this by opening connections to
|       the target web server and sending a partial request. By doing so, it starves
|       the http server's resources causing Denial Of Service.
|
|     Disclosure date: 2009-09-17
|     References:
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-6750
|_      http://ha.ckers.org/slowloris/
| http-sql-injection:
|   Possible sqli for queries:
|     http://192.168.1.13:80/?C=M%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=N%3bO%3dD%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=D%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=S%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=N%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=D%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=M%3bO%3dD%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=S%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=M%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=N%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=D%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=S%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=M%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=N%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=D%3bO%3dD%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=S%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/IG/?C=M%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/IG/?C=S%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/IG/?C=N%3bO%3dD%27%20OR%20sqlspider
|     http://192.168.1.13:80/IG/?C=D%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=M%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=S%3bO%3dD%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=N%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=D%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=M%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=N%3bO%3dD%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=D%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=S%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=M%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=N%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=D%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=S%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=M%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=N%3bO%3dA%27%20OR%20sqlspider
|     http://192.168.1.13:80/?C=D%3bO%3dA%27%20OR%20sqlspider
|_    http://192.168.1.13:80/?C=S%3bO%3dA%27%20OR%20sqlspider
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
|_http-trace: TRACE is enabled
106/tcp  open  pop3pw
110/tcp  open  pop3
|_sslv2-drown:
135/tcp  open  msrpc
139/tcp  open  netbios-ssn
143/tcp  open  imap
|_sslv2-drown:
443/tcp  open  https
| http-csrf:
| Spidering limited to: maxdepth=3; maxpagecount=20; withinhost=192.168.1.13
|   Found the following possible CSRF vulnerabilities:
|
|     Path: https://192.168.1.13:443/project/Login_Form/index.html
|     Form id:
|     Form action: ../index.html
|
|     Path: https://192.168.1.13:443/project/Gaming/index.html
|     Form id: idformregister
|     Form action: https://www.themvp.in/index.php?route=account/register/ajax
|
|     Path: https://192.168.1.13:443/project/Registration/index.html
|     Form id:
|_    Form action: ../Login_form/index.html
|_http-dombased-xss: Couldn't find any DOM based XSS.
| http-enum:
|   /: Root directory w/ listing on 'apache/2.4.41 (win64) openssl/1.1.1c php/7.3.9'
|   /phpmyadmin/: phpMyAdmin
|   /icons/: Potentially interesting folder w/ directory listing
|   /licenses/: Potentially interesting directory w/ listing on 'apache/2.4.41 (win64) openssl/1.1.1c php/7.3.9'
|   /server-info/: Potentially interesting folder
|_  /server-status/: Potentially interesting folder
| http-fileupload-exploiter:
|
|     Couldn't find a file-type field.
|
|_    Couldn't find a file-type field.
| http-slowloris-check:
|   VULNERABLE:
|   Slowloris DOS attack
|     State: LIKELY VULNERABLE
|     IDs:  CVE:CVE-2007-6750
|       Slowloris tries to keep many connections to the target web server open and hold
|       them open as long as possible.  It accomplishes this by opening connections to
|       the target web server and sending a partial request. By doing so, it starves
|       the http server's resources causing Denial Of Service.
|
|     Disclosure date: 2009-09-17
|     References:
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-6750
|_      http://ha.ckers.org/slowloris/
| http-sql-injection:
|   Possible sqli for queries:
|     https://192.168.1.13:443/?C=N%3bO%3dD%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=D%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=S%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=M%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=M%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=D%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=S%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=N%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/IG/?C=S%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/IG/?C=M%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/IG/?C=D%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/IG/?C=N%3bO%3dD%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=M%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=D%3bO%3dD%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=S%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=N%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=M%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=S%3bO%3dD%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=D%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=N%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=D%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=M%3bO%3dD%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=S%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=N%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=N%3bO%3dD%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=D%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=S%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/?C=M%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/IG/?C=N%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/IG/?C=M%3bO%3dA%27%20OR%20sqlspider
|     https://192.168.1.13:443/IG/?C=S%3bO%3dD%27%20OR%20sqlspider
|_    https://192.168.1.13:443/IG/?C=D%3bO%3dA%27%20OR%20sqlspider
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
|_http-trace: TRACE is enabled
| ssl-dh-params:
|   VULNERABLE:
|   Diffie-Hellman Key Exchange Insufficient Group Strength
|     State: VULNERABLE
|       Transport Layer Security (TLS) services that use Diffie-Hellman groups
|       of insufficient strength, especially those using one of a few commonly
|       shared groups, may be susceptible to passive eavesdropping attacks.
|     Check results:
|       WEAK DH GROUP 1
|             Cipher Suite: TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA
|             Modulus Type: Safe prime
|             Modulus Source: RFC2409/Oakley Group 2
|             Modulus Length: 1024
|             Generator Length: 8
|             Public Key Length: 1024
|     References:
|_      https://weakdh.org
|_sslv2-drown:
445/tcp  open  microsoft-ds
902/tcp  open  iss-realsecure
912/tcp  open  apex-mesh
3306/tcp open  mysql
|_mysql-vuln-cve2012-2122: ERROR: Script execution failed (use -d to debug)
|_sslv2-drown:

Host script results:
|_samba-vuln-cve-2012-1182: Could not negotiate a connection:SMB: Failed to receive bytes: ERROR
|_smb-vuln-ms10-054: false
|_smb-vuln-ms10-061: Could not negotiate a connection:SMB: Failed to receive bytes: ERROR

Nmap done: 1 IP address (1 host up) scanned in 350.89 seconds
```
<br>

> Taking advantages of TELNET

- Telnet is a remote management service that runs on port 23.
- Telnet didn't use encrypting communication.
- Telnet is replaced by SSH.
- If telnet is used by any device, we can sniff traffic and recover usernames and password as the communication is not encrypted.
- We can achieve this using Wireshark (packet and protocol analyzer).

*Practical Done*
<br><br>

> Searching for & understanding exploits

- Next step in attack launching is searching exploits.
- Typically, you will be using public exploits.
- You can search these exploits in variety of ways...
    1. [Exploit_DB](https://www.exploit-db.com/)
    2. Searchsploit(tool in kali)
    ```sh
    # Searchsploit

    ┌──(kali㉿kali)-[~]
    └─$ searchsploit vsftpd                                                                                         2 ⨯
    ------------------------------------------------------------------------------ ---------------------------------
    Exploit Title                                                                  |  Path
    ------------------------------------------------------------------------------ ---------------------------------
    vsftpd 2.0.5 - 'CWD' (Authenticated) Remote Memory Consumption                 | linux/dos/5814.pl
    vsftpd 2.0.5 - 'deny_file' Option Remote Denial of Service (1)                 | windows/dos/31818.sh
    vsftpd 2.0.5 - 'deny_file' Option Remote Denial of Service (2)                 | windows/dos/31819.pl
    vsftpd 2.3.2 - Denial of Service                                               | linux/dos/16270.c
    vsftpd 2.3.4 - Backdoor Command Execution (Metasploit)                         | unix/remote/17491.rb
    ------------------------------------------------------------------------------- ---------------------------------
    Shellcodes: No Results

    # Copy exploit
    
    ┌──(kali㉿kali)-[~]
    └─$ searchsploit -m 17491.rb
    ```
<br>

> Understanding Exploits

- You want to amke sure that you understand what it is doing and that it is setup correctly.
- This is to ensure that you will not cause any harm to the system you are launching that exploit.

**Dissecting Exploit**

- Open it in any text editor or search online to see what it is doing.
- You might need to edit the shellcode in order to make it work properly.
- You may also need to understand what kind of arguments it will need to work properly.
<br><br>

> Launching Exploit

- Once we determine how our exploit work and that they aren't maliciour, we can finally launch our attack!
- You typically will do this by executing the exploit file from the command line using the appropriate arguments.
<br><br>

## Metasploit

- Popular hacking tool
- One-stop-shop
- Less control and less flexible
```ruby
┌──(kali㉿kali)-[~]
└─$ msfconsole
                                                  

      .:okOOOkdc'           'cdkOOOko:.
    .xOOOOOOOOOOOOc       cOOOOOOOOOOOOx.
   :OOOOOOOOOOOOOOOk,   ,kOOOOOOOOOOOOOOO:
  'OOOOOOOOOkkkkOOOOO: :OOOOOOOOOOOOOOOOOO'
  oOOOOOOOO.    .oOOOOoOOOOl.    ,OOOOOOOOo
  dOOOOOOOO.      .cOOOOOc.      ,OOOOOOOOx
  lOOOOOOOO.         ;d;         ,OOOOOOOOl
  .OOOOOOOO.   .;           ;    ,OOOOOOOO.
   cOOOOOOO.   .OOc.     'oOO.   ,OOOOOOOc
    oOOOOOO.   .OOOO.   :OOOO.   ,OOOOOOo
     lOOOOO.   .OOOO.   :OOOO.   ,OOOOOl
      ;OOOO'   .OOOO.   :OOOO.   ;OOOO;
       .dOOo   .OOOOocccxOOOO.   xOOd.
         ,kOl  .OOOOOOOOOOOOO. .dOk,
           :kk;.OOOOOOOOOOOOO.cOk:
             ;kOOOOOOOOOOOOOOOk:
               ,xOOOOOOOOOOOx,
                 .lOOOOOOOl.
                    ,dOd,
                      .

       =[ metasploit v6.0.15-dev                          ]
+ -- --=[ 2071 exploits - 1123 auxiliary - 352 post       ]
+ -- --=[ 596 payloads - 45 encoders - 10 nops            ]
+ -- --=[ 7 evasion                                       ]

Metasploit tip: Search can apply complex filters such as search cve:2009 type:exploit, see all the filters with help search

msf6 > 
```
*Practial Done*
<br><br>

## Bruteforce attacks

- Outside of launching attacks with exploits, you could simply be able to brute force your way into the system.

- By launching a bruteforce attack, you are continuously attempting to login to a specific account (typically) with a large number of passwords you have in a wordlist until you are granted access.

**THC HYDRA**

THC Hydra is a program that comes built-in in kali linux that allows you to launch brute force attacks.

*Syntax*
```sh
hydra -l username -P wordlist.txt [Ip address] [login method]

# example: 

hydra -l root -P wordlist.txt 10.10.10.2 ssh
```

**John-the-ripper**

John the ripper is another password cracking tool that can crack password hashes.
```sh
john --wordlist=[wordlist] [hashed password file]

# example:

john --wordlist=list myhashes.txt
```
<br>

## ARP Spoofing

- Address Resolution Protocol(ARP) allows computers to associate MAC address with IP address.

- You have to be in same network range in order to do this attack.

- Using ARP spoofing, you can impersonate another computer an potentially intercept information like creds. that were meant to go somewhere else.
```md
# Mannual ARP spoofing

aprspoof -i [interface] -t [target_source] [target_destination]

# Example

arpspoof -i eth0 -t 10.0.0.15 10.0.0.1(gateway_ip)

arpspoof -i eth0 -t 10.0.0.1 10.0.0.15

> NOTE: You have to do perform this twice, first to fool the client and then to fool the router(gateway).
> NOTE: You have to enable IP_forwording if you are doing this in linux

# IP_Forwording

echo 1 > /proc/sys/net/ipv4/ip_forword

----------------------------------------------------------------------------------------------------------------------------------------
# ARP spoofing using bettercap(tool)

sudo bettercap -iface eth0
10.0.0.0/24 > 10.0.0.4 >> 

# Common Modules and commands:

10.0.0.0/24 > 10.0.0.4 >> net.probe on
10.0.0.0/24 > 10.0.0.4 >> net.recon on
10.0.0.0/24 > 10.0.0.4 >> net.show

10.0.0.0/24 > 10.0.0.4 >> set arp.spoof.fullduplex true
10.0.0.0/24 > 10.0.0.4 >> set arp.spoof.targets 10.0.0.15
10.0.0.0/24 > 10.0.0.4 >> arp.spoof on

10.0.0.0/24 > 10.0.0.4 >> net.sniff on
```
<br>

## Intorduction to cryptography

- Cryptography is the science or study of protecting information, weather in transit or in rest, by using techniques to render the information unusable to anyone who does not possess the means to decrypt it.

- In our case, it simple means taking plain text like "Hello Ayush" and transform it into an unreadable formate like "Uryyb Nlhfu"

- This is achive using  encryption algorithms

**Encryption Algorithms**

- Encryption algorithms are simply mathematical formulas that can encrypt and decrypt data using a specific key.

- Without the key, the message will be forever unreadable and will not be able to be decrypted.

- The two types of encryption are:

  - **Symmetric encryption**, which uses the same key for encryption and decryption, provide confidentiality but not non-repudiation.

  - **Asymmetric encryption**, which uses one to encrypt an another key to decrypt. Typically the recipient's public key encrypts data that they are sent and their private key decrypts the data so they can view.

**Block Ciphers Vs. Stream Ciphers**

*Chipers are the methods by which data is encrypted.*

|        **Stream Cipher**        |        **Block Cipher**        |
|:-------------------------------:|:------------------------------:|
|In stream ciphers, bits of data are encrypted as a continuous stream. This results in faster speeds.|In block ciphers, bits of data are put into blocks (i.e. 64 bit blocks) and are encrypted with an algorithm and key one block at a time. This results in slow speeds of encryption but it is reliable|
<br>

**Symmetric Encryption Algorithms**

- DES & 3DES (Data Encryption Standard)
  - Block cipher
  - 56 bit key
  - 8 bits reserved for parity

- AES (Advanced Encryption Standard)
  - Block cipher
  - 128/192/256 bits key
  - Replaces DES

- Twofish
  - Block cipher
  - 256 bit key

- Blowfish
  - Fast cipher
  - 64 bit block size
  - 32 to 448 bit key

- RC (Rivest Cipher)
  - Block cipher
  - Upto 2040 bits key
  - 128 bit block size
<br>

**Asymmetric Encryption Algorithms**

- Diffie Hellman
  - Key exchange protocol
  - Uses SSL/IPSec

- ECC (Elliptic Curve Cryptosystem)
  - Uses point to an elliptical curve, in conjuction with logarithmic problems, for encryption and signatures.
  - Uses less processing power.
  - Good for mobile devices.

- RSA (Rivest, Shamir, Adleman)
  - Strong encryption
  - Use two large prime numbers
  - 4096 bits key
  - Current standard

**Hashing Algorithms**

- Hashing algorithms can br thought of as form of encryption that cannot br reversed.

- As a result, hashes are a good way to ensure that the integrity of file/ messages have remained intact.

- You typically use hashing algorithms to securely transmit and store things like passwords
<br>

**Specific Hashing Algorithms**

* **MD5** --> Produces a 128 bit hash value output, expressed as a 32-digit hexadecimal.

* **SHA-1** --> Produces a 160 bit hash value output.

* **SHA-2** --> Holds 4 seperate hash functions that produce outputs of 224,256,384 and 512 bits. It is worth noting that SHA-2 is not as widely used as SHA-1.

**Attacking Hashes**

- While hashed are secure, they are not immune to being attacked.

- Hashes can be cracked in various ways, including comparing a discovered hash to the hashes of a list strings in a file.

- This is pretty achievable with application like hashcat and john-the-ripper in kali linux.

- They are also vulnerable to what is known as "Collisions", aka, the hash of two separate strings coincidentally being equivalent.

**Protecting Hashes**

- There is something called a salt that can be added to a hash to add an extra layer of security.

- *Salting a hash* is when extra bits of data are added onto a hash to prevent the original hash form being cracked and further reducing the chance of the plaintext version being uncovered.

**Cryptography Attacks**

*There a few ways that cryptography can be attacked:*

- **Known plaintext attack** - attacker has both ciphertext and plain text and uses them both to figure out how it was encrypted.

- **Ciphertext-only-attack** - attack where the hacker has multiple messages encrypted in the same way and does a statistical analysis on them ot find out how they were encrypted.

- **Replay attack** - man in the middle attack where an attacker sits between two computers, captures a cryptographic exchange, and repeats it in hopes of gaining access. This is not effective when session tokens are used.