# Section-3 Hacking Labs

A network that you create that lets you practice your skills in a controlled environment.
<br><br>

> What is virtualization?

Virtualization is the precess of creating a software-based, or virtual representation of something, such as virtual applications, servers, storage and networks.

It enable us to run more than one operating system in a single computer.
<br><br>

> Hardware specs:

* Atlest 20% free disk space.
* 8GB of RAM(min).
* Software needed:
    - Virtual box
    - VMware workstation
    - Hyper-V
<br><br>

> Kali linux:

* OS used for hacking
* Open source
* Created by offensive security

> Lab setup:

*Every virtual machine mentioned below is in a isolated private network range- [10.0.0.0], and using Network Address Translation(NAT)*

**Virtual Machines:**
1. Kali linux
2. Metasploitable
3. Server C and Server 2012
4. Windows 10
5. Windows 7